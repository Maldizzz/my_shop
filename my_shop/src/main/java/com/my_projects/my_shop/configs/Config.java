package com.my_projects.my_shop.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan("com.my_projects.my_shop")
public class Config implements WebMvcConfigurer {

    public void addResourceHandler(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/css/")
                .addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/images/")
                .addResourceLocations("file:images/");
    }
}
