package com.my_projects.my_shop.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private int price;

    @OneToOne(mappedBy = "product", fetch = FetchType.LAZY)
    private ProductImage productImage;

    public ProductImage getProductImage() {
        if(productImage == null) {
            return new ProductImage();
        }
        return productImage;
    }
}
