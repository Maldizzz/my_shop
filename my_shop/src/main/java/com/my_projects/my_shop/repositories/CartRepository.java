package com.my_projects.my_shop.repositories;

import com.my_projects.my_shop.entities.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface CartRepository extends CrudRepository<Cart, Long> {
}
