package com.my_projects.my_shop.repositories;

import com.my_projects.my_shop.entities.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    List<Product> findAll();

    Optional<Product> findOneById(Long id);

    List<Product> findOneByName(String name);
}
