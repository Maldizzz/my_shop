package com.my_projects.my_shop.repositories;

import com.my_projects.my_shop.entities.CartItem;
import org.springframework.data.repository.CrudRepository;

public interface CartItemRepository extends CrudRepository<CartItem, Long> {
}
