package com.my_projects.my_shop.repositories;

import com.my_projects.my_shop.entities.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class InMemoryProductRepository implements ProductRepository {

    ArrayList<Product> products;

    public InMemoryProductRepository() {
//        products = new ArrayList<>();
//        products.add(new Product(1L, "Коффе с молоком", 50));
//        products.add(new Product(2L, "Коффе со льдом", 50));
//        products.add(new Product(3L, "Глясе", 70));
//        products.add(new Product(4L, "Еспрессо", 55));
//        products.add(new Product(5L, "Каппучино", 80));
//        products.add(new Product(6L, "Брив", 90));
//        products.add(new Product(7L, "Мокачино", 100));
    }

    @Override
    public List<Product> findAll() {
        return products;
    }

    @Override
    public Optional<Product> findOneById(Long id) {
        return products.stream()
                .filter(product -> product.getId() == id)
                .findFirst();
    }

    @Override
    public List<Product> findOneByName(String name) {
        return null;
    }
}
