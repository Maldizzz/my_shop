package com.my_projects.my_shop.repositories;

import com.my_projects.my_shop.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByName(@Param(value = "name") String name);

    List<Product> findAllByNameLike(@Param(value = "name") String name);

    void deleteProductById(@Param(value = "id") Long id);
}
