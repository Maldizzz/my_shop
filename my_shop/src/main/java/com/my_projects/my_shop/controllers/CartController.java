package com.my_projects.my_shop.controllers;

import com.my_projects.my_shop.entities.Cart;
import com.my_projects.my_shop.services.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/cart")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @GetMapping("")
    public String show(Model model, @CookieValue(name = "cart") Cookie cookie,
                       HttpServletResponse response) {
        Cart cart = cartService.getCart(cookie, response);
        model.addAttribute("items", cart.getItems());
        model.addAttribute("price", cart.getPrice());
        return "cart";
    }

    @GetMapping("/add")
    public void addProductToCart(@RequestParam(value = "id") long productId,
                                 @CookieValue(name = "cart", required = false) Cookie cookie,
                                 HttpServletResponse response,
                                 HttpServletRequest request) throws IOException {
        cartService.addProduct(productId, cookie, response);
        response.sendRedirect(request.getHeader("referer"));
    }
}
