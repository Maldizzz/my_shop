package com.my_projects.my_shop.controllers;

import com.my_projects.my_shop.entities.Product;
import com.my_projects.my_shop.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/all")
    public String products(Model model) {
        Iterable<Product> products = productService.findAll();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("{id}")
    public String productsById(@PathVariable Long id, Model model) {
        Product by = productService.findBy(id);
        model.addAttribute("products", by);
        return "products";
    }

    @GetMapping("/byName/{name}")
    public String productsById(@PathVariable String name, Model model) {
        List<Product> by = productService.findByName(name);
        model.addAttribute("products", by);
        return "products";
    }

    @GetMapping("/add")
    public String addProduct(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("typeOperation", "Добавление товара");
        return "addProduct";
    }

    @GetMapping("/edit")
    public String editProduct(Model model, @RequestParam long id) {
        Product product = productService.findBy(id);
        model.addAttribute("product", product);
        return "editProduct";
    }

    @PostMapping("/saveProduct")
    public String saveProduct(@ModelAttribute(name = "product") Product product) {
        productService.saveProduct(product);
        return "redirect:/products/all";
    }

    @GetMapping("/deleteProduct")
    public String deleteProduct(@RequestParam long id) {
        productService.deleteProduct(id);
        return "redirect:/products/all";
    }
}
