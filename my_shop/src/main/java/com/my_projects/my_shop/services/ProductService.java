package com.my_projects.my_shop.services;

import com.my_projects.my_shop.entities.Product;
import com.my_projects.my_shop.repositories.JpaProductRepository;
import com.my_projects.my_shop.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    private final JpaProductRepository jpaProductRepository;

    public Iterable<Product> findAll() {
        return jpaProductRepository.findAll();
    }

    public Product findBy(long id) {
        Optional<Product> oneById = jpaProductRepository.findById(id);
        return oneById.orElse(null);
    }

    public List<Product> findByName(String name) {
        return jpaProductRepository.findAllByNameLike("%" + name + "%");
    }

    public void addProduct(Product product) {
        jpaProductRepository.save(product);
    }

    public void saveProduct(Product product) {
        jpaProductRepository.save(product);
    }

    public void deleteProduct(long id) {
        Optional<Product> byId = jpaProductRepository.findById(id);
        byId.ifPresent(jpaProductRepository::delete);
    }
}
