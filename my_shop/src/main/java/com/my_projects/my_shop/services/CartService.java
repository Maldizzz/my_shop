package com.my_projects.my_shop.services;

import com.my_projects.my_shop.entities.Cart;
import com.my_projects.my_shop.entities.CartItem;
import com.my_projects.my_shop.entities.Product;
import com.my_projects.my_shop.repositories.CartItemRepository;
import com.my_projects.my_shop.repositories.CartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@Service
public class CartService {

    private final CartRepository cartRepository;

    private final ProductService productService;

    private final CartItemRepository cartItemRepository;

    public void addProduct(long productId, Cookie cookie, HttpServletResponse response) {
        Cart cart = getCart(cookie, response);
        Product product = productService.findBy(productId);
        CartItem newItem = cartItemRepository.save(new CartItem(product, cart));
        cart.getItems().add(newItem);
        cartRepository.save(cart);
    }

    public Cart getCart(Cookie cookie, HttpServletResponse response) {
        Cart cart = null;
        if (cookie != null) {
            long idCart = Long.parseLong(cookie.getValue());
            cart = cartRepository.findById(idCart).orElse(null);
        }
        if (cart == null) {
            cart = cartRepository.save(new Cart());
            cookie = new Cookie("cart", cart.getId().toString());
            cookie.setPath("/app");
            response.addCookie(cookie);
        }
        return cart;
    }
}
