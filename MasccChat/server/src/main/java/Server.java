import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Server {
    private List<ClientHandler> clients = new ArrayList<>();

    private DBAuthService authService;

    public Server() {
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            System.out.println("Сервер запустился");
            authService = new DBAuthService();
            while (true) {
                Socket socket = serverSocket.accept();
                new ClientHandler(socket, this);
                System.out.println("подключился новый клиент");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AuthService getAuthService() {
        return authService;
    }

    public void broadcastMsg(String msg) {
        clients.forEach(clientHandler -> clientHandler.sendMsg(msg));
    }

    public void subscribe(ClientHandler clientHandler) {
        clients.add(clientHandler);
        broadcastClientsList();
    }

    public void unsubscribe(ClientHandler clientHandler) {
        clients.remove(clientHandler);
        broadcastClientsList();
    }

    public boolean nicknameIsBusy(String nickname) {
        return clients.stream().anyMatch(clientHandler -> clientHandler.getNickname().equals(nickname));
    }

    public void broadcastClientsList() {
        StringBuilder sb = new StringBuilder(10 * clients.size());
        sb.append("/clients ");
        for (ClientHandler client : clients) {
            sb.append(client.getNickname()).append(" ");
        }
        sb.setLength(sb.length() - 1);
        broadcastMsg(sb.toString());
    }

    public void privateMsg(String writingMsgNickname, String nickReceiver, String msg) {
        Optional<ClientHandler> ch = clients.stream()
                .filter(clientHandler -> clientHandler.getNickname().equals(nickReceiver))
                .findFirst();
        if (writingMsgNickname.equals(nickReceiver)) {
            ch.ifPresent(clientHandler -> clientHandler.sendMsg("Сообщение себе: \n" + msg));
        } else {
            ch.ifPresent(clientHandler -> clientHandler.sendMsg("Вам личное сообщение от " +
                    writingMsgNickname + ": \n" + msg));
        }
    }

    public void changeNameUser(String oldName, String newName) {
        if (authService.changeNickUser(oldName, newName)) {
            broadcastClientsList();
            broadcastMsg("Пользователь с ником: " + oldName + " изменил ник на: " + newName);
        }
    }

    public boolean changePassUser(String nickname, String oldPass, String newPass) {
        return authService.changePassUser(nickname, oldPass, newPass);
    }

    public boolean changeLoginUser(String nickname, String oldLogin, String newLogin) {
        return authService.changeLoginUser(nickname, oldLogin, newLogin);
    }
}

