public interface AuthService {

    String getNicknameByLoginAndPassword(String login, String password);

    boolean changeNickUser(String oldName, String newName);
}