import java.sql.*;

public class DBAuthService implements AuthService {
    private Connection connection;
    private Statement statement;

    public DBAuthService() {
        connect();
    }

    @Override
    public String getNicknameByLoginAndPassword(String login, String password) {
        String query = "select nickname from users where login ='" + login + "' and password ='" + password + "';";
        String nickname = null;
        try {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                nickname = resultSet.getString("nickname");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nickname;
    }

    public boolean changeNickUser(String oldName, String newName) {
        String query = "update users set nickname = '" + newName + "' where nickname = '" + oldName + "';";
        int result = 0;
        try {
            result = statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result == 1;
    }

    public boolean changePassUser(String nickname, String oldPass, String newPass) {
        String query = "update users set password = '" + newPass + "'" +
                " where nickname = '" + nickname + "' and password = '" + oldPass + "';";
        int result = 0;
        try {
            result = statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result == 1;
    }

    private void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:main.db");
            statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public boolean changeLoginUser(String nickname, String oldLogin, String newLogin) {
        String query = "update users set login = '" + newLogin + "'" + " where nickname = '" +
                nickname + "' and login = '" + oldLogin + "';";
        int result = 0;
        try {
            result = statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result == 1;
    }
}
