import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler {
    private Socket socket;
    private Server server;
    private DataOutputStream out;
    private DataInputStream in;
    private String nickname = "";


    public ClientHandler(Socket socket, Server server) {
        try {
            this.socket = socket;
            this.server = server;
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());

            new Thread(() -> {
                try {
                    while (true) {
                        String msg = in.readUTF();
                        if (msg.startsWith("/auth ")) {
                            String[] tokens = msg.split("\\s", 3);
                            String nick = server.getAuthService().getNicknameByLoginAndPassword(tokens[1], tokens[2]);
                            if (nick == null) {
                                sendMsg("/authbad такого логина и пароля нет");
                            } else if (server.nicknameIsBusy(nick)) {
                                sendMsg("/authbad такой пользователь уже авторизирован");
                            } else {
                                nickname = nick;
                                sendMsg("/authok " + nick);
                                server.subscribe(this);
                                break;
                            }
                        }
                    }

                    while (true) {
                        String msg = in.readUTF();
                        if (msg.startsWith("/")) {
                            if (msg.equals("/end")) {
                                sendMsg("/end");
                                server.broadcastMsg("Клиент с ником " + nickname + " вышел из чата.");
                                break;
                            }
                            if (msg.startsWith("/changeName ")) {
                                String newName = msg.split("\\s", 2)[1];
                                String oldName = this.nickname;
                                this.nickname = newName;
                                server.changeNameUser(oldName, newName);
                                sendMsg(msg);
                            }
                            if (msg.startsWith("/p ")) {
                                String oldPass = msg.split("\\s", 3)[1];
                                String newPass = msg.split("\\s", 3)[2];
                                if (server.changePassUser(nickname, oldPass, newPass)) {
                                    sendMsg("Вы сменили свой пароль." + "\n" + "Ваш новый пароль " + newPass + ".");
                                } else {
                                    sendMsg("Вы ввели не верный пароль!");
                                }
                            }
                            if (msg.startsWith("/m ")) {
                                String[] tokens = msg.split("\\s", 3);
                                String nickReceiver = tokens[1];
                                String writingMsgNickname = this.nickname;
                                server.privateMsg(writingMsgNickname, nickReceiver, tokens[2]);
                            }
                            if (msg.startsWith("/l ")) {
                                String oldLogin = msg.split("\\s", 3)[1];
                                String newLogin = msg.split("\\s", 3)[2];
                                if (server.changeLoginUser(nickname, oldLogin, newLogin)) {
                                    sendMsg("Вы изменили логин с " + oldLogin + " на " + newLogin + ".");
                                } else {
                                    sendMsg("Вы ввели не верный логин" + " либо такой логин уже есть");
                                }
                            }
                        } else {
                            server.broadcastMsg(this.nickname + ": " + msg);
                        }
                    }
                } catch (IOException e) {
                    System.out.println("клиент вышел из чата");
                } finally {
                    disconnect();
                }
            }).start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getNickname() {
        return nickname;
    }

    public void sendMsg(String msg) {
        try {
            out.writeUTF(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disconnect() {
        server.unsubscribe(this);
        System.out.println("клиент вышел из чата");
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


