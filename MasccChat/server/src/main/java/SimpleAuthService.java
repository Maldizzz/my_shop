import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SimpleAuthService implements AuthService {
    private class UserData {
        private String nickname;
        private final String login;
        private final String password;

        public UserData(String login, String password, String nickname) {
            this.nickname = nickname;
            this.login = login;
            this.password = password;
        }
    }

    private List<UserData> userDataList = new ArrayList<>();

    public SimpleAuthService() {
        for (int i = 0; i < 10; i++) {
            userDataList.add(new UserData("login" + i, "pass" + i, "user" + i));
        }
    }

    @Override
    public String getNicknameByLoginAndPassword(String login, String password) {
        return userDataList.stream()
                .filter(userData -> userData.login.equals(login) && userData.password.equals(password))
                .map(userData -> userData.nickname)
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean changeNickUser(String oldName, String newName) {
        Optional<UserData> first = userDataList.stream()
                .filter(userData -> userData.nickname.equals(oldName))
                .findFirst();

        if (first.isPresent()) {
            first.get().nickname = newName;
            return true;
        } else {
            return false;
        }
    }
}