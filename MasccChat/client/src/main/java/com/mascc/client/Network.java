package com.mascc.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Network {
    private static Network instance;

    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    public Callback onReceiveMsg;
    public Callback onAuthOk;
    public Callback onAuthBad;
    public Callback onDisconnect;
    public Callback onException;

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    private Network() {
        Callback empty = args -> {
        };
        onReceiveMsg = empty;
        onDisconnect = empty;
        onAuthBad = empty;
        onAuthOk = empty;
        onException = empty;
    }

    public void sendAuth(String login, String pass) {
        try {
            if (socket == null || socket.isClosed()) {
                connect();
            }
            dataOutputStream.writeUTF("/auth " + login + " " + pass);
        } catch (IOException e) {
            onException.callback("Не получается подключиться к серверу");
        }
    }

    public void sendMsg(String msg) {
        try {
            dataOutputStream.writeUTF(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connect() throws IOException {
        socket = new Socket("Localhost", 8189);
        dataInputStream = new DataInputStream(socket.getInputStream());
        dataOutputStream = new DataOutputStream(socket.getOutputStream());

        Thread thread = new Thread(() -> {
            try {
                while (true) {
                    String msg = dataInputStream.readUTF();
                    if (msg.startsWith("/authok ")) {
                        onAuthOk.callback(msg.split("\\s", 2)[1]);
                        break;
                    } else if (msg.startsWith("/authbad ")) {
                        String info = msg.split("\\s", 2)[1];
                        onAuthBad.callback(info);
                    }
                }

                while (true) {
                    String msg = dataInputStream.readUTF();
                    if (msg.equals("/end")) {
                        break;
                    }
                    onReceiveMsg.callback(msg + "\n");
                }
            } catch (IOException e) {
                onException.callback("Соединение с сервером разорвано!");
            } finally {
                disconnect();
            }
        });
        thread.setDaemon(true);
        thread.start();
    }


    private void disconnect() {
        onDisconnect.callback();
        try {
            dataInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            dataOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}