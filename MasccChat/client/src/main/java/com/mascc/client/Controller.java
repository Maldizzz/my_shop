package com.mascc.client;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    HBox authPanel, msgPanel;
    @FXML
    TextField loginField;
    @FXML
    PasswordField passField;
    @FXML
    TextArea textArea;
    @FXML
    TextField msgField;
    @FXML
    ListView<String> clientsList;
    @FXML
    Label name;

    private boolean isAuth;

    private String nickname;

    public void setAuth(boolean auth) {
        isAuth = auth;
        authPanel.setVisible(!auth);
        authPanel.setManaged(!auth);
        msgPanel.setVisible(auth);
        msgPanel.setManaged(auth);
        clientsList.setVisible(auth);
        clientsList.setManaged(auth);
    }

    public void sendMsg() {
        String text = msgField.getText();
        if (text.trim().isEmpty()) {
            return;
        }
        Network.getInstance().sendMsg(text);
        msgField.clear();
        msgField.requestFocus();
    }

    public void sendAuth() {
        Network.getInstance().sendAuth(loginField.getText(), passField.getText());
        loginField.clear();
        passField.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setAuth(false);
        clientsList.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                String nickname = clientsList.getSelectionModel().getSelectedItem();
                msgField.setText("/m " + nickname + " ");
                msgField.requestFocus();
                msgField.selectEnd();
            }
        });
        linkCallbacks();
    }

    private void linkCallbacks() {
        Network.getInstance().onReceiveMsg = args -> {
            String msg = args[0].toString();
            if (msg.startsWith("/clients ")) {
                String[] tokens = msg.split("\\s");
                Platform.runLater(() -> {
                    clientsList.getItems().clear();
                    for (int i = 1; i < tokens.length; i++) {
                        clientsList.getItems().add(tokens[i]);
                    }
                });
            } else if (msg.startsWith("/changeName ")) {
                String windowName = msg.split("\\s", 2)[1];
                Platform.runLater(() -> name.setText(windowName));
            } else textArea.appendText(msg);
        };
        Network.getInstance().onAuthOk = args -> {
            nickname = args[0].toString();
            Platform.runLater(() -> name.setText(nickname));
            setAuth(true);
        };
        Network.getInstance().onAuthBad = args -> showAlert(args[0].toString());
        Network.getInstance().onDisconnect = args -> setAuth(false);
        Network.getInstance().onException = args -> showAlert(args[0].toString());
    }

    private void showAlert(String arg) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING, arg, ButtonType.OK);
            alert.showAndWait();
        });
    }
}