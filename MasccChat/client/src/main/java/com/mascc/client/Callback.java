package com.mascc.client;

public interface Callback {

    void callback(Object... args);
}